Librarie maison créée avec un ❤ de 🍑️ par Grégoire

# Dev en cours :
- [ ] Changement du css des points
- [x] Affichage des marqueurs complets
- [x] Fix marqueurs size
- [x] Fix css colors points with Judi
- [x] Fix css colors markeurs with Judi
- [x] Marker = Picto ?
- [x] change markeur box shadow to dark one
- [ ] SI indicateurs vide pas de pts undefined
- [ ] Trouver astuce valeur de points hover
- [ ] si undefined pas afficher le pt
- [ ] minimum si le pt est à zero
- [ ] TTaille depts minimum quand 0

# 1. Installation

La librarie est composée de 3 élements (voir exemple de code pour plus de compréhension):

- Un fichier CSS `graph-vis.css` à importer dans le `<head>`
- Du code HMTL à rajouter dans le `<body>` avant l'import du fichier JS
- Un Fichier Javascript `graph-visualisation.js` à placer **avant son appel en javascript** 

Le container va prendre automatiquement l'espace disponible autour de lui.
disponible

## Structure des fichiers et du code en HTML 
```html
<!-- Container HTML à placer avant l'appel de la lib -->
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- 1. CSS a mettre dans le head -->
    <link rel="stylesheet" type="text/css" href="PATH-TO-CSS/graph-vis.css"/>
</head>
<body>
    <!-- 2. Container HTML à placer dans le body -->
    <div class="graph-container">
        <div class="graph"></div>
    </div>
    
    <!-- 3. Appel de la librarie après le container HTML -->
    <script src="PATH-TO-LIB/graph-visualisation.js"></script>
</body>

```

# 2. Utilisation & subtilités

## Données du graph

Les données doivent être définis grâce à la propriété `graphVis.data` et avec le format suivant :

```javascript
 graphVis.data = [
    // Un objet par pays
    {
        flagURL: "PathToTheFlagImage",
        country: "NameOfTheCountry️",
        //Liste des indicateurs un par objet
        indicators: [
            {
                name: "Masque porté quotodiennement",
                data: [
                    {
                        //date au format timestamp
                        date: 1519211809934,
                        // pourcentage entre 0 et 100 ou undefined si pas de données
                        percent: 50
                    }
                ]
            },
        ],
    }
];
```

## Ajout de marqueurs (Claim et Validation)

Les marqueurs sont de types `{ 'claim', 'valid'}`

Ils sont ajoutés au fur à mesure. Pour supprimer tous les markeurs en cours `graphVis.clearMarkers();`

```javascript
// Pour reset les markeurs
graphVis.clearMarkers();
//ajout de plusieurs markeurs
graphVis.setMarker(timestamp, type = 'claim');
graphVis.setMarker(timestamp, type = 'claim');
```

## Message par défaut

Le message par défaut est modifiable par l'attribut `graphVis.defaultMessage`, il peut être soit : 
- un type string
- soit un type Element

`refreshData()`  **doit être appelé pour le mettre à jour**

```javascript
graphVis.defaultMessage = 'Je suis une message par defaut';
graphVis.defaultMessage = '<div class="loading">Hello je suis une message</div>';
graphVis.refreshData();
```

## Mise à jour du graph

La mise à jour du graphique est lancé par :

```javascript
graphVis.refreshData();
```
