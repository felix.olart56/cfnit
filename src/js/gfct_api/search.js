const getSearchResults = async (text_input, language_code, next_page_token = '') => {
    const results = { error: null };
    if(text_input !== '') {
        results.data = await gfctSearch(text_input, language_code, next_page_token);
        if(Object.keys(results.data).length === 0)
            results.error = { message: 'No data available', code: 2};
    }
    else {
        results.data = null;
        results.error = { message: 'Input text is empty', code: 1};
    }
    return results;
}

const handleKeyPressedSearchBar = async (event, input) => {
    if(event.code === 'Enter' || event.code === 'NumpadEnter'){
        const language_code = document.querySelector('.language-selector').value;
        const results = await getSearchResults(input.value, language_code);
        cleanContainer(document.querySelector('.gfct-search-results'));
        buildSearchResults(results);
    }
}

const handleOnClickNextPageButton = async (next_page_token) => {
    const language_code = document.querySelector('.language-selector').value;
    const text_input = document.querySelector('.search-bar-input').value;
    const results = await getSearchResults(text_input, language_code, next_page_token);
    buildSearchResults(results);
}

const handleOnClickCloseSearchWindow = (node) => {
    node.parentNode.style['display'] = 'none';
}

const gfctSearch = async (value, language, next_page_token, page_size = 9) => {
    let search_params = {'query': value};
    if(language !== '')
        search_params.languageCode = language;
    if(next_page_token != '')
        search_params.pageToken = next_page_token;
    search_params.pageSize = page_size;
    return gapi.client.factchecktools.claims.search(search_params)
    .then((response) => JSON.parse(response.body))
    .catch((error) => console.log(error));
}