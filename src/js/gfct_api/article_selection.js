const handleOnClickSelectButton = (node) => {
    const claim = node.value;
    const selected_article_container = document.querySelector('.selected-article');
    if(selected_article_container.children.length > 0)
        cleanContainer(selected_article_container);
    selected_article_container.appendChild(buildNews(claim, 'change'));
    document.querySelector('.search-window').style.display = 'none';
    setMarkers();
    graphVis.refreshData();
}

const handleOnClickChangeButton = () => {
    document.querySelector('.search-window').style.display = 'flex';
}