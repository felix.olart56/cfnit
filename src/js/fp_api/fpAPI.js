class fpAPI {

    static country_codes;
    // Because there are several countries that are named differently
    // flag_api_name: cwsod_api_name
    static name_differencies = {
        'Czechia': 'Czech Republic',
        "Côte d'Ivoire (Ivory Coast)": "Côte d'Ivoire",
        'Puerto Rico': 'Puerto Rico, U.S.',
        'United States': 'United States of America',
    }

    static getCountryCodes = async () => {
        return fetch('https://flagcdn.com/en/codes.json')
            .then((response) => response.json())
            .then((data) => {
                return Object.keys(data).reduce((ret, key) => { // Invert 'code: name' to 'name: code'
                    if(this.name_differencies[data[key]] !== undefined)
                        ret[this.name_differencies[data[key]]] = key;
                    else
                        ret[data[key]] = key;
                    return ret;
                }, {});
            })
            .catch((error) => console.log(error));
    };

    static getFlagUrl = (country_code) => 'https://flagcdn.com/256x192/' + country_code + '.png';

    static getFlagUrlByCountryName = (name) =>
        (fpAPI.country_codes[name] !== undefined && fpAPI.country_codes[name] !== null)
        ? { data: { url: this.getFlagUrl(fpAPI.country_codes[name]) }, error: null }
        : { data: null, error: { code: 1, message: 'No flag available for: ' + name } };

};