const getVisualizationData = async (sel_indics, sel_countries, daterange) => {
    if(sel_indics.length === 1 && sel_countries.length > 0) {
        return getDataMultpileCountriesOneIndicator(sel_indics[0], sel_countries, daterange, 'daily')
        .then((response) => formatDataToGraphVis(response))
        .catch((error) => console.log(error));
    }
    else if(sel_indics.length > 0 && sel_countries.length === 1) {
        return getDataOneCountryMultipleIndicators(sel_indics, sel_countries[0], daterange, 'daily')
        .then((response) => formatDataToGraphVis([response]))
        .catch((error) => console.log(error));
    }
    return null;
}
    
const getDataMultpileCountriesOneIndicator = (indicator, countries, daterange, type,
    result = [], iterator = { current_id: 0, max_id: countries.length - 1 }) =>
    cwsodAPI.getDataOnIndicator({ indicator, country: countries[iterator.current_id], daterange, type })
    .then((response) => {
        result.push(
            (response.data !== undefined && response.data !== null && response.data.length > 0)
            ? { country: countries[iterator.current_id], data: [{ indicator: indicator, data: dateStuffing(response.data, indicator), error: null }] }
            : { country: countries[iterator.current_id], data: [{ indicator: indicator, data: null, error: { message: 'No data available' } }] }
        )
        if(iterator.current_id === iterator.max_id)
            return result
        else {
            iterator.current_id++;
            return getDataMultpileCountriesOneIndicator(indicator, countries, daterange, type, result, iterator);
        }
    });

const getDataOneCountryMultipleIndicators = async (indicators, country, daterange, type,
    result = { country: country, data: [] }, iterator = { current_id: 0, max_id: indicators.length - 1 }) =>
    cwsodAPI.getDataOnIndicator({ indicator: indicators[iterator.current_id], country, daterange, type })
    .then((response) => {
        result.data.push(
            (response.data !== undefined && response.data !== null && response.data.length > 0)
            ? { indicator: indicators[iterator.current_id], data: dateStuffing(response.data, indicators[iterator.current_id]), error: null }
            : { indicator: indicators[iterator.current_id], data: null, error: { message: 'No data available' } }
        )
        if(iterator.current_id === iterator.max_id)
            return result
        else {
            iterator.current_id++;
            return getDataOneCountryMultipleIndicators(indicators, country, daterange, type, result, iterator);
        }
    });


const formatDataToGraphVis = (data) => (data !== undefined && data !== null)
    ? data.map((country) => {
        const flag_url = fpAPI.getFlagUrlByCountryName(country.country);
        return {
            flagURL: (flag_url.error === null) ? flag_url.data.url : undefined,
            country: (country.country !== undefined) ? country.country : undefined,
            indicators: formatIndicatorsToGraphVis(country.data)
        }
    })
    : [ { error: { message: 'No data available' } } ]

const formatIndicatorsToGraphVis = (data) => (data !== null && data !== undefined)
    ? data.map((indicator) => {
        const indic_data = ((cwsodAPI.getIndicDataByIndicId(indicator.indicator)).error === null)
        ? (cwsodAPI.getIndicDataByIndicId(indicator.indicator)).data.indicator
        : { name: undefined, special_name: undefined };
        return {
            name: indic_data.name,
            data: (indicator.error === null)
            ? indicator.data.map((info) => {
                const percent = info[(indic_data.special_name !== undefined) ? 'percent_' + indic_data.special_name : 'pct_' + indic_data.indicator]
                return {
                    date: surveyDateToTimestamp(info.survey_date),
                    percent: (percent !== undefined) ? Math.trunc(100 * percent) : undefined
                }
            })
            : []
        };
    })
    : [ { error: { message: 'No data available' } } ]

const surveyDateToTimestamp = (survey_date) => {
    const year = survey_date.slice(0, 4);
    const month = ('0' + (parseInt(survey_date.slice(4, 6)) - 1)).slice(-2);
    const day = ('0' + (parseInt(survey_date.slice(6, 8)) + 1)).slice(-2);
    return (new Date(year, month, day)).getTime();
}

const dateStuffing = (data, indicator) => {
    let indic_data = cwsodAPI.getIndicDataByIndicId(indicator);
    indic_data = (indic_data.error === null) ? indic_data.data.indicator : { special_name: undefined, name: indicator };
    const dates = getSelectedDateRange().map((date) => surveyDateToTimestamp(date));
    const step = 1000 * 60 * 60 * 24; // one day in milliseconds
    const number_of_days = Math.ceil(1 + (dates[1] - dates[0]) / step);

    if(data.length !== number_of_days) {
        let old_data_index = 0;
        const new_data = new Array(number_of_days).fill(null).map((date, index) => formatDate(dates[0] + index * step, ''))
        return new_data.map((date) => {
            const current_old_data = data.find((element) => element.survey_date === date);
            if(current_old_data !== undefined) {
                old_data_index++;
                return JSON.parse(JSON.stringify(data[old_data_index - 1]));
            }
            else {
                const new_date = JSON.parse(JSON.stringify(data[0]));
                new_date[((indic_data.special_name !== undefined) ? indic_data.special_name : indic_data.indicator) + '_se'] = undefined;
                new_date[((indic_data.special_name !== undefined) ? indic_data.special_name : indic_data.indicator) + '_se_unw'] = undefined;
                new_date[(indic_data.special_name !== undefined) ? 'percent_' + indic_data.special_name : 'pct_' + indic_data.indicator] = undefined;
                new_date[(indic_data.special_name !== undefined) ? 'percent_' + indic_data.special_name + '_unw' : 'pct_' + indic_data.indicator + '_unw'] = undefined;
                new_date.sample_size = undefined;
                new_date.survey_date = date;
                return new_date;
            }
        });
    }
    return data;
}

