const green_log = 'background-color: #00a603; font-weight: bold; color: white;';
const red_log = 'background-color: #a60000; font-weight: bold; color: white;';
const GFCT_API = 'gfct_api';
const CWSOD_API = 'cwsod_api';
let keys = null;

const getLocalFile = (url) => {
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.open('GET', url, false);
    xmlhttp.send();
    if(xmlhttp.status === 200)
        return xmlhttp.responseText;
    return { error: "File not found : " + url };
};

const onPageLoad = async () => {
    keys = JSON.parse(getLocalFile('src/js/api_keys.json'));
    cwsodAPI.indicators = JSON.parse(getLocalFile('src/js/cwsod_api/indicators.json'));
    buildIndicatorsList(cwsodAPI.indicators);
    buildCountryList(await cwsodAPI.getAvailableCountries());
    document.querySelector('.selected-article').appendChild(buildNews(null));
    fpAPI.country_codes = await fpAPI.getCountryCodes();
};

const cleanContainer = (container) => {
    while(container.firstChild)
        container.removeChild(container.firstChild);
};